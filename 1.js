
let firstPass = document.querySelector('.first-pass')
let firstIcon = document.querySelector('.icon-password-first')
firstIcon.classList.add('fa-eye-slash')

firstIcon.addEventListener('click', ()=>{
    if (firstPass.getAttribute('type') === 'password'){
        firstPass.setAttribute('type','text');
        firstIcon.classList.remove('fa-eye-slash')
    }else{
        firstPass.setAttribute('type', 'password');
        firstIcon.classList.add('fa-eye-slash')
    }
})


let secondPass = document.querySelector('.second-pass')
let secondIcon = document.querySelector('.icon-password-second')

secondIcon.classList.add('fa-eye-slash')
secondIcon.addEventListener('click', ()=>{
    if (secondPass.getAttribute('type') === 'password'){
        secondPass.setAttribute('type','text');
        secondIcon.classList.remove('fa-eye-slash')
    }else{
        secondPass.setAttribute('type', 'password');
        secondIcon.classList.add('fa-eye-slash')
    }
})


let btn = document.querySelector('.btn')
btn.addEventListener('click', ()=>{
    if (firstPass.value === secondPass.value){
        alert('You are welcome')
    } else {
        alert('Your password is wrong')
    }
})
